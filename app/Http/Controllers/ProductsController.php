<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;

class ProductsController extends Controller
{
    public function index()
    {
        $product = Products::all()->toArray();
        return response()->json($product);
    }

    public function store(Request $request)
    {
        try{
    		$product = new Products([
    			'name'=>$request->input('name'),
                'sku'=>$request->input('sku'),
                'stock'=>$request->input('stock'),
                'category_id'=>$request->input('category_id'),
    			]);
    		$product->save();
    		return response()->json(['status'=>true, 'Producto creado'], 200);
    	} catch (\Exception $e){
    		Log::critical("No se ha podido añadir: {$e->getCode()} , {$e->getLine()} , {$e->getMessage()}");
    		return response('Someting bad', 500 );
    	}
    }

    public function show($id)
    {
        try{
    		$product = Products::find($id);
    		if(!$product){
    			return response()->json(['No existe el producto'], 404);
    		}
    		
    		return response()->json($product, 200);
    	} catch (\Exception $e){
    		Log::critical("No se ha podido encontrar el producto: {$e->getCode()} , {$e->getLine()} , {$e->getMessage()}");
    		return response('Someting bad', 500 );
    	}
    }

    public function update(Request $request, $id)
    {
         try{
    		$product = Products::find($id);
    		if(!$product){
    			return response()->json(['No existe...'], 404);
    		}
    		
            $product->update($request->all());
    		return response(array(
                'error' => false,
                'message' =>'Producto Modificado',
               ),200);
    	} catch (\Exception $e){
    		Log::critical("No se ha podido editar: {$e->getCode()} , {$e->getLine()} , {$e->getMessage()}");
    		return response('Someting bad', 500 );
    	}
    }

    public function destroy($id)
    {
        try{
    		$product = Products::find($id);
    		if(!$product){
    			return response()->json(['No existe ese producto'], 404);
    		}
    		
    		$product->delete();
    		return response()->json('Producto eliminado..', 200);
    	} catch (\Exception $e){
    		Log::critical("No se ha podido eliminar: {$e->getCode()} , {$e->getLine()} , {$e->getMessage()}");
    		return response('Someting bad', 500 );
    	}
    }
}

